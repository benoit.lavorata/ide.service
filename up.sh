#!/bin/bash
echo ""
echo "Coder IDE"
echo "Maintainer: Benoit Lavorata <blavorata@gmail.com>"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export DOCKER_HOST_IP=$(ip -4 addr show docker0 | grep -Po 'inet \K[\d.]+')
    export DOCKER_HOST_USER=$(whoami)
    export HOST_PWD=$PWD
    export HOST_UID=$UID
    export $(cat .env | xargs)
 
    echo "DOCKER_HOST_IP = $DOCKER_HOST_IP"
    echo "DOCKER_HOST_USER = $DOCKER_HOST_USER"
    echo "HOST_PWD = $HOST_PWD"
    echo "HOST_UID = $UID"

    echo ""
    echo "Create networks ..."
    docker network create $IDE_CONTAINER_NETWORK

    echo ""
    echo "Boot ..."
    docker-compose up -d --remove-orphans $1
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

